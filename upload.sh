#!/bin/bash -e

HOST=${1:?"Specify host as first argument"}
API=${HOST}/api/v1
KEY=${2:?"Specify API key as second argument"}
CURL="curl -sS --fail"
AUTH="-H Authorization:${KEY}"
JSON="-H Content-Type:application/json"

echo Creating sources...
USER=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"users"}' | jq -r .Id`
echo USER source: ${USER}
ITEM=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"items"}' | jq -r .Id`
echo ITEM source: ${ITEM}
INTERACTION=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"interactions"}' | jq -r .Id`
echo INTERACTION source: ${INTERACTION}
USER_CLUSTER=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"user_clusters"}' | jq -r .Id`
echo USER_CLUSTER source: ${USER_CLUSTER}
ITEM_CLUSTER=`${CURL} -X POST "${API}/sources" ${AUTH} ${JSON} -d '{"Name":"item_clusters"}' | jq -r .Id`
echo ITEM_CLUSTER source: ${ITEM_CLUSTER}
CLUSTER_META=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"user_clusters_meta"}' | jq -r .Id`
echo CLUSTER_META source: ${CLUSTER_META}
GENRE_MOVIE=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"genre_movie"}' | jq -r .Id`
echo GENRE_MOVIE source: ${GENRE_MOVIE}
GENRE=`${CURL} -X POST "${API}/sources" ${AUTH} -d '{"Name":"genres"}' | jq -r .Id`
echo GENRE source: ${GENRE}

echo Uploading users...
${CURL} -X POST "${API}/sources/${USER}/id/upload-csv" ${AUTH} -F file=@users.csv.gz
echo
echo Uploading movies...
${CURL} -X POST "${API}/sources/${ITEM}/id/upload-csv" ${AUTH} -F file=@movies.csv.gz
echo
echo Uploading interactions...
${CURL} -X POST "${API}/sources/${INTERACTION}/id/upload-csv" ${AUTH} -F file=@transactions.csv.gz
echo
echo Uploading user clusters...
${CURL} -X POST "${API}/sources/${USER_CLUSTER}/id/upload-csv" ${AUTH} -F file=@user_clusters.csv.gz
echo
echo Uploading item clusters...
${CURL} -X POST "${API}/sources/${ITEM_CLUSTER}/id/upload-csv" ${AUTH} -F file=@movie_clusters.csv.gz
echo
echo Uploading clusters meta...
${CURL} -X POST "${API}/sources/${CLUSTER_META}/id/upload-csv" ${AUTH} -F file=@user_clusters_meta.csv
echo
echo Uploading genres...
${CURL} -X POST "${API}/sources/${GENRE}/id/upload-csv" ${AUTH} -F file=@genres.csv.gz
echo
echo Uploading genre_movie...
${CURL} -X POST "${API}/sources/${GENRE_MOVIE}/id/upload-csv" ${AUTH} -F file=@genres_movies.csv.gz
echo

echo "Waiting 30 seconds for source processing. (can be fixed if we want)"
sleep 30
BODY='{"Name":"users", "ProcessingEnabled":true, "Fields":[{"Type":"int64","Identifier":"cluster", "SourceId":'${USER_CLUSTER}', "IdentifierInSource":"cluster_id"},{"Type":"int64", "Identifier":"edits", "SourceId":'${USER}', "IdentifierInSource":"num_edits"}]}'
echo $BODY
curl -X POST "${API}/buckets" ${AUTH} -d "$BODY"

echo Creating buckets...
USER=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"users", "ProcessingEnabled":true, "Fields":[{"Type":"int64","Identifier":"cluster", "SourceId":'${USER_CLUSTER}', "IdentifierInSource":"cluster_id"},{"Type":"int64", "Identifier":"edits", "SourceId":'${USER}', "IdentifierInSource":"num_edits"}]}' | jq -r .Id`
echo USER bucket: ${USER}
ITEM=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"items", "ProcessingEnabled":true, "Fields":[{"Type":"int64","Identifier":"cluster", "SourceId":'${ITEM_CLUSTER}', "IdentifierInSource":"cluster_id"},{"Type":"string", "Identifier":"title", "SourceId":'${ITEM}', "IdentifierInSource":"english_title"}]}' | jq -r .Id`
echo ITEM bucket: ${ITEM}
INTERACTION=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"interactions", "ProcessingEnabled":true, "Fields":[{"Type":"epoch", "Identifier":"timestamp", "SourceId":'${INTERACTION}', "IdentifierInSource":"ts"},{"Type":"int64", "Identifier":"user", "SourceId":'${INTERACTION}', "IdentifierInSource":"user_id"},{"Type":"int64", "Identifier":"item", "SourceId":'${INTERACTION}', "IdentifierInSource":"product_id"}]}' | jq -r .Id`
echo INTERACTION bucket: ${INTERACTION}
CLUSTER_META=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"clusters", "ProcessingEnabled":true, "Fields":[{"Type":"string", "Identifier":"name", "SourceId":'${CLUSTER_META}', "IdentifierInSource":"name"}]}' | jq -r .Id`
echo CLUSTER_META bucket: ${CLUSTER_META}
GENRE=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"genres", "ProcessingEnabled":true, "Fields":[{"Type":"string", "Identifier":"name", "SourceId":'${GENRE}', "IdentifierInSource":"name"}]}' | jq -r .Id`
echo GENRE bucket: ${GENRE}
GENRE_MOVIE=`${CURL} -X POST "${API}/buckets" ${AUTH} -d '{"Name":"genre_movie", "ProcessingEnabled":true, "Fields":[{"Type":"int64", "Identifier":"genre", "SourceId":'${GENRE_MOVIE}', "IdentifierInSource":"genre_id"},{"Type":"int64", "Identifier":"item", "SourceId":'${GENRE_MOVIE}', "IdentifierInSource":"movie_id"}]}' | jq -r .Id`
echo GENRE_MOVIE bucket: ${GENRE_MOVIE}


echo Creating data model...
BODY='{"Name":"data_model", "Root":{"Node":{"Bucket": '${INTERACTION}',"ChildValues":[{"FieldName":"user","Node":{"Bucket":'${USER}', "ChildValues":[{"FieldName":"cluster", "Node": {"Bucket":'${CLUSTER_META}'}}]}},{"FieldName":"item","Node":{"Bucket":'${ITEM}',"ChildSets": [{"FieldName":"genres", "ParentFieldName": "item", "Node": {"Bucket": '${GENRE_MOVIE}', "ChildValues":[{"FieldName":"genre", "Node": {"Bucket": '${GENRE}'}}]}}], "ChildValues":[{"FieldName":"cluster", "Node": {"Bucket":'${CLUSTER_META}'}}]}}]},"UserField":"user","ItemField":"item","TimestampField":"timestamp"}}'
echo $BODY

DATAMODEL=`${CURL} -X POST "${API}/data-models" ${AUTH} -d "$BODY" | jq -r .Id`
echo Data model: ${DATAMODEL}



